const { Builder, By, Key, until } = require("selenium-webdriver");
const chrome = require("selenium-webdriver/chrome");
const fs = require("fs");
const helpers = require("./helpers.js");

const saveScreenshot = async (driver, filename) => {
  return driver.takeScreenshot().then((data) => {
    fs.writeFile(
      filename,
      data.replace(/^data:image\/png;base64,/, ""),
      "base64",
      function (err) {
        if (err) throw err;
      }
    );
  });
};

module.exports.takeScreen = async (userName) => {
  try {
    let driver = await new Builder()
      .forBrowser("chrome")
      .setChromeOptions(new chrome.Options().addArguments('--disable-dev-shm-usage').addArguments('--no-sandbox').headless())
      .build();
    await driver.get(`https://livecounts.io/tiktok-realtime/${userName}`);
    try {
      await driver.wait(until.titleIs("webdriver - Google Search"), 3000);
    } catch(e) {

    }

    try {
      let elements = await driver.findElements(
        By.className(
          "bg-red-400 hover:bg-red-500 focus:outline-none text-white px-4 py-2 rounded-lg w-full mt-2"
        )
      );
      await elements[0].click();
      
      await driver.wait(until.titleIs("webdriver - Google Search"), 3000);
    } catch(e) {

    }

    let filename = `${helpers.getRandomString(100000000, 999999999)}.jpg`
    await saveScreenshot(driver, filename);
    await driver.quit();
    return filename
  } catch(e) {
    console.log(e)
  }
   
};
