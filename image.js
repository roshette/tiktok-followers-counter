require("dotenv").config();
const sharp = require("sharp");

module.exports.sharp = async (screenshotName = "screen.png") => {
  try {
    await sharp(screenshotName)
      .extract({
        width: parseInt(process.env.IMG_WIDTH),
        height: parseInt(process.env.IMG_HEIGHT),
        left: 0,
        top: parseInt(process.env.IMG_TOP),
      })
      .toFile(`crop${screenshotName}`);
  } catch (e) {
    console.log("Sharp error", e);
  }
};
