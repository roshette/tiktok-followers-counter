const Tesseract = require("tesseract.js");

module.exports.getNumbers = async (screenshotName = "screen.jpg") => {
  try {
    let data = await Tesseract.recognize(`crop${screenshotName}`, "eng", {
      tessedit_char_whitelist: "0123456789",
    });

    return data;
  } catch (e) {
    console.log(e);
  }
};
