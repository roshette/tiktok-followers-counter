const mongoose = require("mongoose");
mongoose.set('useFindAndModify', false); 

const Schema = mongoose.Schema;
const userScheme = new Schema({
    name: String,
    subscribers: Number,
    token: String,
    dateRequest: Number,
});

module.exports = mongoose.model("User", userScheme);