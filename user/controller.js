const User = require("./model");

exports.getFollowers = async (name) => {
  let user = await User.findOne({ name: name });

  if (!user) {
    user = new User({
      name: name,
      subscribers: 0,
      token: "",
      dateRequest: Math.floor(Date.now() / 1000),
    });

    try {
        user = await user.save();
    } catch(e) {

    }
  }

  return user.subscribers;
};

exports.updateFollowers = async (name, count) => {
  try {
    const user = await User.findOneAndUpdate(
      { name: name },
      { subscribers: count },
      { new: true }
    );

    return user;
  } catch (e) {
    console.log(e);
    return "Error!";
  }
};
