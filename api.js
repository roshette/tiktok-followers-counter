const site = require('./selenium.js');
const image = require('./image.js');
const tesseract = require('./number.js');
const fs = require("fs");

module.exports.getSubscribeCount = async (nickname) => {
    if(!nickname) {
        return 'Need pass nickname!'
    }

    let filename = await site.takeScreen(nickname);
    await image.sharp(filename);
    let { data } = await tesseract.getNumbers(filename);

    fs.unlinkSync(`${filename}`)
    fs.unlinkSync(`crop${filename}`)

    return data.text
}