const express = require("express");
const mongoose = require("mongoose");
const api = require("./api.js");
const app = express();
const cors = require("cors");
const port = 3000;

const User = require("./user/controller");

app.use(cors());

app.get("/getSubscribeCount/:nickname", async (req, res) => {
  if (!req.params.nickname) {
    res.send("500");
  }

  try {
    let subscribersCount = await api.getSubscribeCount(req.params.nickname);
    res.send(subscribersCount);
  } catch (e) {
    console.log(e);
    res.send("Some error!");
  }
});

app.get("/", async (req, res) => {
  let followers = await User.getFollowers("valery");
  res.send(followers + "");
});

app.get("/t", async (req, res) => {
  let user = await User.updateFollowers("valery", 10);
  res.send("ok")
});

mongoose.connect(
  "mongodb://localhost:27017/tiktok",
  { useNewUrlParser: true, useUnifiedTopology: true },
  function (err) {
    if (err) return console.log(err);
    app.listen(port, () => {
      console.log(`Example app listening at http://localhost:${port}`);
    });
  }
);
